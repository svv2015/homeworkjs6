function createNewUser() {
    const newUser = {
        firstName: prompt("Введіть ім'я"),
        lastName: prompt("Введіть прізвище"),
        getLogin() {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        getAge() {
            const birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy");
            const birthYear = birthday.split(".")[2];
            const currentYear = new Date().getFullYear();
            return currentYear - birthYear;
        },
        getPassword() {
            const firstLetter = this.firstName[0].toUpperCase();
            const lastNameLower = this.lastName.toLowerCase();
            const birthYear = prompt("Введіть рік народження");
            return `${firstLetter}${lastNameLower}${birthYear}`;
        }
    };
    return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());